# APRSC

Are you interested in running [APRSC](https://github.com/hessu/aprsc), but in a containerized environment? This repository is for you!

## Running APRSC in Docker

```bash
docker run registry.gitlab.com/memhamwan/netops/aprsc:latest
```

This is currently unversioned -- you can reference a specific commit SHA as a tag or `latest`.

## Using Helm

This chart is published to GitLab CI's package repository. Access that below:

```bash
https://gitlab.example.com/api/v4/projects/38823731/packages/helm/devel
```

Note that this requires GitLab credentials in your command. If you don't have gitlab credentials or just want to avoid the registry entirely, use the following commands to clone and install:

```bash
git clone https://gitlab.com/memhamwan/netops/aprsc.git
cp aprsc/helm/values.yaml values.yaml
helm dependency update ./aprsc/helm
# You'll want to edit the values.yaml file to suit your usecase! To see a real-world example, view aprsc/deploy/values.yaml
helm install -f values.yaml aprsc ./aprsc/redis
```


In the future, we hope to add automation to publish this directly to a charts repository that is publicly readable.

Currently the chart is 0.1.0, and the chart is being redeployed under this same semver in the devel channel routinely. Treat this as effectively unversioned code at this point.

## Can I see an example?

Sure! [aprs.memhamwan.net](http://aprs.memhamwan.net:14501/) is running configured fully from this repository, GitLab CI, and an on-premise K8S cluster running on servers in Memphis, TN, USA on [MemHamWAN](https://www.memhamwan.org). Trace it backwards from the `/deploy` directory to the `/helm` directory and finally the `Dockerfile`. A [separate repository](https://gitlab.com/memhamwan/netops/ioc-terraform/) is used to configure the DNS, and finally [another repo](https://gitlab.com/memhamwan/netops/cluster-management) is used to bootstrap the on-prem cluster. Between these three projects, all of the configuration is captured for reference (less secrets hidden away in GitLab CI variables).

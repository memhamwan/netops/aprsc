FROM debian:bullseye-slim

ARG APRSC_VERSION=2.1.11
ARG GPG_AGENT_VERSION=2.2.27
ARG DIRMNGR_VERSION=2.2.27
ARG GPG_VERSION=2.2.27

LABEL org.opencontainers.image.authors="netops@memhamwan.org"
LABEL org.opencontainers.image.source="https://gitlab.com/memhamwan/netops/aprsc/"
LABEL org.opencontainers.image.version="${APRSC_VERSION}"
LABEL org.opencontainers.image.title="aprsc"
LABEL org.opencontainers.image.description="an APRS-IS server in C, in a third-party docker container"

RUN apt-get update && \
    apt-get install -y \
    "gpg=${GPG_VERSION}*" \
    "dirmngr=${DIRMNGR_VERSION}*" \
    "gpg-agent=${GPG_AGENT_VERSION}*" \
    --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN echo "deb [signed-by=/usr/share/keyrings/aprsc.gpg] http://aprsc-dist.he.fi/aprsc/apt bullseye main" > /etc/apt/sources.list.d/aprsc.list && \
    gpg --keyserver keyserver.ubuntu.com --recv C51AA22389B5B74C3896EF3CA72A581E657A2B8D && \
    gpg --export C51AA22389B5B74C3896EF3CA72A581E657A2B8D > /usr/share/keyrings/aprsc.gpg

RUN apt-get update && \
    apt-cache policy aprsc && \
    apt-get install -y \
    "aprsc=${APRSC_VERSION}*" \
    --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

COPY test/test-config.conf /opt/aprsc/etc/test-config.conf

USER aprsc

EXPOSE 10152/tcp
EXPOSE 10152/udp
EXPOSE 14580/tcp
EXPOSE 14580/udp
EXPOSE 8080/tcp
EXPOSE 8080/udp
EXPOSE 14501/tcp

WORKDIR /opt/aprsc

HEALTHCHECK CMD curl -f http://localhost:14501/ || exit 1

ENTRYPOINT ["/opt/aprsc/sbin/aprsc"]

CMD ["-e", "debug", "-o", "stderr", "-r", "logs", "-c", "/opt/aprsc/etc/aprsc.conf"]
